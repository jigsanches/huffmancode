#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "huffman.h"
/*  
	@author: Gabriel C. Sanches
	date: June 15th, 2018
	Data Structure/ Compression code
*/

/****************************************************
* newNode() recebe uma tupla de char e freq e aloca *
* um heapNode e o retorna							*
*****************************************************/
heapNode * newNode(tuple t){
	heapNode * aux = malloc(sizeof(heapNode));
	aux->data = t.data;
	aux->freq = t.freq;
	aux->left = NULL;
	aux->right = NULL;
	return aux;
}

/****************************************************
* extractMin() extraí o menor valor da heap e rees- *
* trutura para que volte a ser uma heap 			*
****************************************************/
heapNode * extractMin(minHeap * heap){
	heapNode * t = heap->nodeArray[0];
	heap->nodeArray[0] = heap->nodeArray[heap->size - 1];
	heap->size -= 1;
	minHeapify(heap, 0);
	return t;
}

/****************************************************
* allocateMinHeap() apenas aloca uma heap que contem*
* um tamanho e capacidade final (default = 94)		*
*****************************************************/
minHeap * allocateMinHeap(int capacidade){
	minHeap * heap = malloc(sizeof(minHeap));
	heap->size = 0;
	heap->capacity = capacidade;
	heap->nodeArray = malloc(capacidade * sizeof(heapNode));
	return heap;
}

/****************************************************
* fullConstructMinHeap simplesmente aloca uma est 	*
* heap, insere os nós dadas as tuplas e depois exec.*
* minHeapify() para tornar o array uma minHeap 		*
*****************************************************/
minHeap * fullConstructMinHeap(tuple t[], int size){
	minHeap * heap = allocateMinHeap(size);
	int i;
	for(i = 0; i < size; ++i){
		heap->nodeArray[i] = newNode(t[i]);
	}
	heap->size = size;
	buildMinHeap(heap);

	return heap;
}

/****************************************************
* buildHuffman() gera uma nova heap e depois faz o 	*
* algoritmo para que somente um nó raiz contendo 	*
* todos os outros seja retornado e então podemos 	*
* fazer a compressão logo após 						*
*****************************************************/
heapNode * buildHuffman(tuple t[], int size){
	// 1º: criar uma heap com as tuplas e o tamanho
	// correspondente
	minHeap * heap = fullConstructMinHeap(t, size);
	heapNode *top, *right, *left; // ponteiros usados
	//guardar referêcia para os nós EXTRAÍDOS e depois
	//para juntá-los e inseri-los novamente na estrutura

	//Enquanto o tamanho da minha Heap não for 1...
	while(heap->size != 1){
		// Extrai os 2 menores valores da minha Heap
		left = extractMin(heap);
		right = extractMin(heap);

		// Cria o novo nó com a soma das freq e depois
		// atribui o filho esq à left e dir à right
		tuple x = {'$', left->freq + right->freq};
		top = newNode(x);
		top->left = left;
		top->right = right;

		insertNode(heap, top);
	}

	// O último nó é certamente o nó pai de todos os outros que
	// já foram mesclados
	return extractMin(heap);
}

/****************************************************
* swapNode() realiza a troca de espaço de memória de*
* 2 nós de uma Heap 								*
*****************************************************/
void swapNode(heapNode ** a, heapNode **b){
	heapNode * t = *a;
	*a = *b;
	*b = t;
}

/****************************************************
* MinHeapify()										*
* dada um estrutura de Heap e uma posição no vetor	*
* de nós, transforma a subarvore em Heap de trás	*
* para frente										*
*****************************************************/
void minHeapify(minHeap * heap, int id){
	int l = 2 * id + 1;
	int r = l + 1;
	int small = id;
	int heapSize = heap->size;
	if(l < heapSize && heap->nodeArray[l]->freq < heap->nodeArray[small]->freq)
		small = l;
	if(r < heapSize && heap->nodeArray[r]->freq < heap->nodeArray[small]->freq)
		small = r;
	if(small != id){
		swapNode(&(heap->nodeArray[small]), &(heap->nodeArray[id]));
		minHeapify(heap, small);
	}
}

/****************************************************
* insertNode é responsável por inserir nós no array *
* da estrutura de Heap e sobe até a raiz de pai em 	*
* pai mantendo a regra de uma minHeap caso o array 	*
* já seja											*
*****************************************************/
void insertNode(minHeap * heap, heapNode * node){
	heap->size += 1;
	int i = heap->size - 1;
	while(i && node->freq < heap->nodeArray[(i - 1) / 2]->freq){
		heap->nodeArray[i] = heap->nodeArray[(i - 1 / 2)];
		i = (i - 1)/2;
	}
	heap->nodeArray[i] = node;
}

/****************************************************
* buildMinHeap() é responsável por, dada uma struct *
* que contem um vetor de heap, heapificamos esse vet*
*****************************************************/
void buildMinHeap(minHeap * heap){
	int i = heap->size - 1;
	int n;
	for(n = (i - 1) / 2; n >= 0; --n){
		minHeapify(heap, n);
	}
}



/****************************************************
* isLeaf() checa se o nó em questão é uma folha ou 	*
* não												*
*****************************************************/
int isLeaf(heapNode * root){
	return !(root->left || root->right);
}

void HuffmanCodes(tuple t[], int size){
	heapNode * root = buildHuffman(t, size);
	int c[50];
	printCodes(root, 0, c);
}

void printCodes(heapNode * root, int pos, int c[]){
	// Assign 0 to left edge and recur
    if (root->left) {
        c[pos] = 0;
        printCodes(root->left, pos + 1, c);
    }
 
    // Assign 1 to right edge and recur
    if (root->right) {
        c[pos] = 1;
        printCodes(root->right, pos + 1, c);
    }

    // If this is a leaf node, then
    // it contains one of the input
    // characters, print the character
    // and its code from arr[]
    if (isLeaf(root)) {
 		int i;
 		for(i = 0; i < pos; ++i){
 			printf("%d", c[i]);
 		}
        printf(" \t:\t %c\n", root->data);
    }
}

void test_huff(heapNode * root){
	heapNode * aux;
	assert(root->data 		== '$');
	assert(root->left->data == 'f');
	aux = root->left;
	assert(isLeaf(aux));
	aux = root->right;
	assert(aux->data 		== '$');
}

void printHeap(minHeap h){
	int i;
	for (i = 0; i < h.size; ++i)
	{
		printf("pos %d = [%c : %d]\n", i, h.nodeArray[i]->data,
			h.nodeArray[i]->freq);
	}
}

int main()
{
	tuple t[] = {	
					{'a', 7},
					{'n', 4},
					{'b', 1},
					{'c', 1},
					/*{' ', 7},
					{'a', 4},
					{'e', 4},
					{'f', 3},
					{'h', 2},
					{'i', 2},
					{'m', 2},
					{'n', 2},
					{'s', 2},
					{'t', 1},
					{'l', 1},
					{'o', 1},
					{'p', 1},
					{'r', 1},
					{'u', 1},
					{'x', 1},*/
				};
/*	char arr[] = { 'a', 'b', 'c', 'd', 'e', 'f' };
    int freq[] = { 5, 9, 12, 13, 16, 45 };
*/
	int  c[50];
	int size = sizeof(t) / sizeof(t[0]);
	minHeap * a = fullConstructMinHeap(t, size);
	printHeap(*a);
	HuffmanCodes(t, size);
	return 0;
}