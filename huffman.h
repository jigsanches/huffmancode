#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#define MAXTREE 100

typedef struct HEAPNODE
{
	char data;
	int freq;
	struct HEAPNODE *left, *right;
}heapNode;

typedef struct MINHEAP
{
	//tamanho da heap, usado p controlar um laço
	int size;
	//capacidade da árvore, no caso será 94 ->chars imprimíveis
	int capacity;
	//a minHeap não será com ponteiros, mas sim em vetor
	heapNode ** nodeArray;
}minHeap;

typedef struct Tupla
{
	char data;
	int freq;
}tuple;

heapNode 	* newNode				(tuple t);
heapNode 	* extractMin			(minHeap * heap);
heapNode 	* buildHuffman			(tuple t[], int size);
minHeap 	* allocateMinHeap		(int capacidade);
minHeap 	* fullConstructMinHeap	(tuple t[], int size);
void 		  swapNode				(heapNode ** a, heapNode **b);
void 		  minHeapify			(minHeap * heap, int id);
void 		  insertNode			(minHeap * heap, heapNode * node);
void 		  buildMinHeap			(minHeap * heap);
void 		  printCodes			(heapNode * root, int pos, int c[]);
void		  HuffmanCodes 			(tuple t[], int size);
int 		  isLeaf				(heapNode * root);
//tuple 		* ReadFile 				(FILE * p);